import { Home } from '@material-ui/icons';
import GrupoEmpresa from 'core/pages/GrupoEmpresa/GrupoEmpresa';
import { Redirect, Route, Router, Switch } from 'react-router-dom';
import history from './core/utils/history';


const Routes = () => (
    <Router history={history}>
        <Switch>
        <Route path="/crud">
                <GrupoEmpresa />
            </Route>
            
        </Switch>
    </Router>
);
export default Routes;