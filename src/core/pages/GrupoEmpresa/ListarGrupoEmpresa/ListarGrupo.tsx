import {  makeRequest } from "core/utils/request";
import { contentResponseGrupo } from "core/utils/Type";
import { useCallback, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Pagination from "core/components/Pagination";
import { toast } from "react-toastify";

const ListarGrupo = () => {
    const [activePage, setActivePage] = useState(0);
    const [name] = useState('');
    const [isLoading, setIsLoading] = useState(false);

    const [responseGruop, setResponseGruop] = useState<contentResponseGrupo>();

    const getGruop = useCallback(() => {
        const params = {
            page: activePage,
            per_page: 3,
            name: name,
        }
        setIsLoading(true);
        makeRequest({ url: `users/${name}`, params, method: 'GET' })
            .then(response => {setResponseGruop(response.data); console.log(response.data);toast.success("Lista carregada!")})
            .catch(() => toast.error("Erro ao carregar lista!"))
            .finally(() => { setIsLoading(false) })
    }, [activePage, name])

    useEffect(() => {
        getGruop();
    }, [getGruop])


    //isso aqui vai servir para quando for necessario realizar uma busca por nome
    // const handleChangeName = (name: string) => {
    //     setName(name);

    // }
    const handleExcluir = (excluir: number) => {
        const confirm = window.confirm('Deseja Realmente excluir esta User?');
        if (confirm) {
            makeRequest({ url: `/users/${excluir}`, method: 'DELETE' })
                .then(() => {
                    toast.info(`User de ID:${excluir} excluido com sucesso!`);
                   // getGruop();
                }
                )
                .catch(() => toast.error("Erro ao excluir User!"))
        }
    }
    return (
        <>
            <div className="container ">
                <div className="meu-base-form p-2 m-5 table-responsive">
                    <div className="text-end">
                        <Link to="/crud/cadastro" className="btn btn-primary "><i className="fa fa-plus" />Adicionar</Link>
                    </div>
                    <table className="table table-hover">
                        <thead >
                            <tr>
                                <th scope="col">First Name</th>
                                <th scope="col">Last Name</th>
                                <th scope="col" className="text-center">Data Alteração</th>
                                <th scope="col">Ações</th>

                            </tr>
                        </thead>
                        <tbody>
                            { responseGruop?.data.map(user => (
                                <>
                                    <tr key={user.id}>
                                        <td>{user.first_name}</td>
                                        <td>{user.last_name}</td>
                                        <td className="text-center">{user.email}</td>
                                        <td className="d-flex"><Link to={`/crud/${user.id}`} ><i className="fa fa-edit fa-2x"></i></Link> <div className="ms-3" onClick={() => handleExcluir(user.id)}><i className="fa fa-trash text-danger fa-2x"></i></div></td>
                                    </tr>

                                </>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
            {responseGruop && <Pagination
                totalPages={responseGruop.total_pages}
                onChange={page => setActivePage(page)}
            />}

        </>
    );

}
export default ListarGrupo;