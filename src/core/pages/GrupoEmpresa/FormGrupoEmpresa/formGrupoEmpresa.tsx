import BaseFormCadastro from "core/components/BaseFormCadastro/baseformcad";
import { makeRequest } from "core/utils/request";
import { Users } from "core/utils/Type";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useParams } from "react-router-dom";
import { toast } from "react-toastify";

type ParamsType = {
    grupoId: string;
}
const CadastroGrupoEmpresa = () => {
    const { register, handleSubmit, formState: { errors }, setValue} = useForm<Users>();
    const [botaoEnviar, setBotaoEnviar] = useState(false);
    const { grupoId } = useParams<ParamsType>();
    const isEditing = grupoId !== 'cadastro';
    const formTitle = isEditing ? "Editar User" : "Cadastrar User";

    const onSubmit = (data: Users) => {
        setBotaoEnviar(true);
        const payload = {
            ...data
        }
        console.log(data);

        makeRequest({ url: isEditing ? `/users/${grupoId}` : '/users/', method: isEditing ? 'PUT' : 'POST', data: payload })
            .then(() => {
                isEditing?toast.info('Grupo Empresa alterado com sucesso!')
                :toast.info('Grupo Empresa cadastrado com sucesso!');  
            })
            .catch(() => {
                toast.error('Erro ao cadastrar Grupo Empresa');
            })
            .finally(() => { setBotaoEnviar(false);
                setValue('first_name','');
                setValue('last_name','');
                setValue('email','');
            })
            
    }
    useEffect(() => {
        if (isEditing) {
            console.log(grupoId);
            setBotaoEnviar(true)
            makeRequest({ url: `/users/${grupoId}` })
                .then(response => {
                    console.log(response);
                setValue('first_name',response.data.data.first_name);
                setValue('last_name',response.data.data.last_name);
                setValue('email',response.data.data.email);
                setBotaoEnviar(false);
                })
                .catch(()=> toast.error("Erro ao consultar Id Informado"))
        }
    }, [grupoId, isEditing, setValue]);

    return (
        <>
            <div className="col-md-6 offset-md-3 col-12">
                <form onSubmit={handleSubmit(onSubmit)}>
                <BaseFormCadastro enviando={botaoEnviar} history={"/crud"} titulo={formTitle}>
                    <div className="p-5">

                        <div className="col-12">
                            <label className="form-text-contato" >First Name</label>
                            <input {...register("first_name", 
                                    { required: "Este campo é obrigatorio" , 
                                        maxLength:{value:8 , message:"Maximo de Caracteres 8"} })} type="text" placeholder="First Name"
                                className={errors.first_name? "form-control input-base is-invalid":"form-control input-base"}></input>
                                {errors.first_name && <span className="text-danger">{errors.first_name?.message}</span>}
                        </div>
                        <div className="col-12">
                            <label className="form-text-contato">Last Name</label>
                            <input  {...register("last_name", 
                                    { required: "Este campo é obrigatorio" , 
                                        maxLength:{value:45 , message:"Maximo de Caracteres 45"} })} type="text" placeholder="Last Name"
                                className={errors.last_name? "form-control input-base is-invalid":"form-control input-base"}></input>
                                {errors.last_name && <span className="text-danger">{errors.last_name.message}</span>}
                        </div>
                        <div className="col-12">
                            <label className="form-text-contato">Email</label>
                            <input  {...register("email", 
                                    { required: "Este campo é obrigatorio" , 
                                        maxLength:{value:45 , message:"Maximo de Caracteres 45"} })} type="text" placeholder="E-mail"
                                className={errors.email? "form-control input-base is-invalid":"form-control input-base"}></input>
                                {errors.email && <span className="text-danger">{errors.email.message}</span>}
                        </div>
                    </div>
                </BaseFormCadastro>
                </form>
            </div>
        </>
    );
}
export default CadastroGrupoEmpresa;