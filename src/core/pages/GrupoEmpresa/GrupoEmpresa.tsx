import Menu from "core/components/Menu/menu";
import {  Route, Switch } from "react-router-dom";
import CadastroGrupoEmpresa from "./FormGrupoEmpresa/formGrupoEmpresa";
import ListarGrupo from "./ListarGrupoEmpresa/ListarGrupo";
import './styles.css';
const GrupoEmpresa = () => {

    return (
        <>
        <Menu/>
            <Switch>
                <Route path="/crud/" exact >
                    <ListarGrupo />
                </Route>
                <Route path="/crud/:grupoId">
                    <CadastroGrupoEmpresa />
                </Route>
            </Switch>
        </>

    );
}
export default GrupoEmpresa;