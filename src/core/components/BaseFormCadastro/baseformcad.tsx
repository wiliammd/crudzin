import ArrowBack from "../ArrowBack/arrowback"
import ButtonEnviar from "../ButtonEnviar/buttonenviar";
import './styles.css';

type Props = {
    titulo: string;
    children: React.ReactNode;
    enviando?: boolean;
    history?: string;

}
const BaseFormCadastro = ({ children, titulo, enviando, history }: Props) => {
    return (
        <div className="m-3">
            <div className="meu-base-form">
                <ArrowBack history={history} />
                <div className="text-center mt-4"><h3>{titulo}</h3></div>
                {children}
                <ButtonEnviar texto={titulo} enviando={enviando} />
            </div>
        </div>
    );
}
export default BaseFormCadastro;