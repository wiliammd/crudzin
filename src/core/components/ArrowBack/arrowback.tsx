import { Link } from "react-router-dom";
import './styles.css';

type Props = {
    history?: string;
}

const ArrowBack = ({history}:Props) => {
    return (
        <div className="da-espaco">
            <div className="container-arrow">
                <Link to={history? history : '/'} className="remover-decoration">
                    <div className="container-arrow">
                        <span className="fas fa-arrow-left fa-2x"></span>
                    </div>
                </Link>
            </div>
        </div>
    );
}
export default ArrowBack;