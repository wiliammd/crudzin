
type Props = {
    texto: string;
    enviando?: boolean;
}
const ButtonEnviar = ({ texto, enviando }: Props) => {
    return (
        <div className="col-12 d-grid text-sm-end d-sm-block p-3">
            <button type="submit" disabled={enviando} className="btn mt-4 border border-1 color-btn" >
                 {texto}
                {
                    enviando &&
                    <>
                        &nbsp;
                        <i className="fas fa-pulse fa-spinner"></i>
                    </>
                }
            </button>
        </div>
    );
}
export default ButtonEnviar;