import './styles.scss'
import ReactPaginate from 'react-paginate';
import { ReactComponent as ArrowIcon } from 'core/assets/images/arrow.svg'

type Props = {
    totalPages:number;
    onChange:(item:number)=>void;
}

const Pagination = ({totalPages,onChange}:Props) =>{

    
    const renderIcon = (className: 'previous' | 'next') =>(
        <ArrowIcon className={`pagination-${className} `} data-testid={`arrow-icon-${className}`}/>
    );

    return(
        <div className="pagination-container">
          
            <ReactPaginate pageCount={totalPages} pageRangeDisplayed={2}
            marginPagesDisplayed={1} onPageChange={selectItem => {onChange(selectItem.selected+1); console.log(selectItem.selected)}}
            previousLabel={renderIcon("previous")}
            nextLabel={renderIcon("next")}
            containerClassName={"pagination"}
            pageLinkClassName={"pagination-item"}
            breakClassName={"pagination-item"}
            activeLinkClassName={"active"}
            previousClassName={"page-active"}
            nextClassName={"page-active"}
            disabledClassName={"page-inactive"}
            />
        </div>
    );
}
export default Pagination;