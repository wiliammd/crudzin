
export type contentResponseGrupo = {
    page: number;
    per_page: number;
    total: number;
    total_pages: number;
    data: Users[];
    support: Support;
}



export type Users ={
    id: number;
    email: string;
    first_name: string;
    last_name: string;
    avatar: string;
}

export type Support ={
    url: string;
    text: string;
}
